CKEDITOR.dialog.add( 'numoDialog', function( editor ) {
    return {
        title: 'Numerator Denominator',
        minWidth: 200,
        minHeight: 150,
        contents: [
            {
                id: 'fraction',
                label: 'Insert Values',
                elements: [
                    {
                        type: 'text',
                        id: 'numerator',
                        label: 'Numerator',
                        validate: CKEDITOR.dialog.validate.notEmpty( "Numerator field cannot be empty." )
                    },
                    {
                        type: 'text',
                        id: 'denominator',
                        label: 'Denominator',
                        validate: CKEDITOR.dialog.validate.notEmpty( "Denominator field cannot be empty." )
                    }
                ]
            }
            /*{
                id: 'tab-adv',
                label: 'Advanced Settings',
                elements: [
                    {
                        type: 'text',
                        id: 'id',
                        label: 'Id'
                    }
                ]
            }*/
        ],
        onOk: function() {
            var dialog = this;

            /*var sup = editor.document.createElement( 'sup' );
			var sub = editor.document.createElement( 'sub' );*/
            /*numo.setAttribute( 'title', dialog.getValueOf( 'tab-basic', 'title' ) );
            numo.setText( dialog.getValueOf( 'tab-basic', 'numo' ) );*/

            /*var id = dialog.getValueOf( 'tab-adv', 'id' );
            if ( id )
                numo.setAttribute( 'id', id );*/
			var numerator=dialog.getValueOf( 'fraction', 'numerator' );
            var denominator=dialog.getValueOf( 'fraction', 'denominator');
            editor.insertHtml("<sup>"+numerator+"</sup>&frasl;<sub>"+denominator+"</sub>&nbsp;");
        }
    };
});