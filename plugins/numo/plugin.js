CKEDITOR.plugins.add( 'numo', {
    icons: 'numo',
    init: function( editor ) {
        editor.addCommand( 'numo', new CKEDITOR.dialogCommand( 'numoDialog' ) );
        editor.ui.addButton( 'Numo', {
            label: 'Insert Fraction',
            command: 'numo',
            toolbar: 'insert'
        });

        CKEDITOR.dialog.add( 'numoDialog', this.path + 'dialogs/numo.js' );
    }
});